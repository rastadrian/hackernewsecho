package com.rastadrian.hackernewsecho.model;

/**
 * An application's voice utterance.
 * Created on 10/25/15.
 *
 * @author Adrian Pena
 */
public class Utterance {
    private String utterance;
    private String repeat;

    public Utterance() {
    }

    public Utterance(String utterance, String repeat) {
        this.utterance = utterance;
        this.repeat = repeat;
    }

    public String getUtterance() {
        return utterance;
    }

    public void setUtterance(String utterance) {
        this.utterance = utterance;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }
}
