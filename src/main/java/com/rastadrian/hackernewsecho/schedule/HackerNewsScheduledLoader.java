package com.rastadrian.hackernewsecho.schedule;

import com.rastadrian.hackernewsecho.ApplicationSession;
import com.rastadrian.hackernewsecho.service.HackerNewsService;
import com.rastadrian.hackernewsecho.service.UtteranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created on 9/30/15.
 *
 * @author Adrian Pena
 */
@Component
public class HackerNewsScheduledLoader {
    private static final long FIXED_DELAY = 30 * 60 * 1000; // 1/2 hour

    @Autowired
    HackerNewsService hackerNewsService;

    @Autowired
    UtteranceService utteranceService;

    @Scheduled(fixedDelay = FIXED_DELAY)
    public void updateHackerNews() {
        if(ApplicationSession.getUtterances() == null) {
            ApplicationSession.setUtteranceMap(utteranceService.loadUtterances());
        }

        try {
            Integer[] index = hackerNewsService.getStoryIndex();

            if(ApplicationSession.getCurrentIndex() != null) {
                int currentCode = getIndexCode(ApplicationSession.getCurrentIndex());
                if(currentCode != getIndexCode(index)) {
                    loadIndex(index);
                } else {
                    System.out.println("News: already loaded.");
                }
            } else {
                System.out.println("News: initial load.");
                loadIndex(index);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadIndex(Integer[] index) throws IOException {
        System.out.println("News: loading...");
        ApplicationSession.setCurrentNews(hackerNewsService.getTopStories(index));
        ApplicationSession.setCurrentIndex(index);
        System.out.println("News: loaded.");
    }

    private int getIndexCode(Integer[] index) {
        int sum = 0;
        if(index != null) {
            for(Integer integer : index) {
                sum += integer;
            }
        }
        return sum;
    }
}
