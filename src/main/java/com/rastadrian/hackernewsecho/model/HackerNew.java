package com.rastadrian.hackernewsecho.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created on 9/28/15.
 * @author Adrian Pena
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HackerNew {
    private int id;
    private String title;
    private String url;
    private String type;

    public HackerNew() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
