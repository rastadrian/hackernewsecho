package com.rastadrian.hackernewsecho.intent;

import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SpeechletResponse;

/**
 * A {@link com.amazon.speech.speechlet.Speechlet} Intent Request handler.
 * Created on 10/25/15.
 *
 * @author Adrian Pena
 */
public interface IntentHandler {

    /**
     * Handles the given {@link IntentRequest}. It will also receive the {@link Session} from the application's
     * session execution.
     * @param intentRequest the intent request.
     * @param session the application's session.
     * @return a {@link SpeechletResponse} entity.
     */
    SpeechletResponse handleIntent(IntentRequest intentRequest, Session session);
}
