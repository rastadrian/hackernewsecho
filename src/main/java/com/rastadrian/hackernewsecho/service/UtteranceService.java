package com.rastadrian.hackernewsecho.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rastadrian.hackernewsecho.ApplicationSession;
import com.rastadrian.hackernewsecho.model.Utterance;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Application's utterance service.
 * Created on 10/25/15.
 *
 * @author Adrian Pena
 */
@Component
public class UtteranceService {

    /**
     * Loads the utterances document.
     * @return a hash with the utterances.
     */
    public Map<String, Utterance> loadUtterances() {
        Resource resource = new ClassPathResource("utterances");
        try {
            File file = resource.getFile();
            if(file != null) {
                return new ObjectMapper().readValue(file, new TypeReference<Map<String, Utterance>>(){});
            }
        } catch (IOException e) {
            //NOP
        }
        return null;
    }

    /**
     * Gets an utterance.
     * @param utteranceId the utterance's code.
     * @return an {@link Utterance}.
     */
    public Utterance getUtterance(String utteranceId) {
        Map<String, Utterance> map = ApplicationSession.getUtterances();
        if(map != null) {
            return map.get(utteranceId);
        }
        return null;
    }
}
