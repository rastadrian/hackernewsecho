package com.rastadrian.hackernewsecho;

import com.rastadrian.hackernewsecho.model.HackerNew;
import com.rastadrian.hackernewsecho.model.Utterance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created on 9/30/15.
 *
 * @author Adrian Pena
 */
public class ApplicationSession {
    public static List<String> log = null;
    private static List<HackerNew> currentNews= null;
    private static Integer[] currentIndex = null;
    private static Map<String, Utterance> utteranceMap = null;

    public static synchronized void setCurrentNews(List<HackerNew> hackerNews) {
        currentNews = hackerNews;
    }

    public static synchronized List<HackerNew> getCurrentNews() {
        return currentNews;
    }

    public static synchronized void setCurrentIndex(Integer[] index) {
        currentIndex = index;
    }

    public static synchronized Integer[] getCurrentIndex() {
        return currentIndex;
    }

    public static synchronized void addLog(String logEntry) {
        if(log == null) {
            log = new ArrayList<>();
        }

        log.add(logEntry);
    }

    public static synchronized void setUtteranceMap(Map<String, Utterance> utterances) {
        utteranceMap = utterances;
    }

    public static Map<String, Utterance> getUtterances() {
        return utteranceMap;
    }
}
