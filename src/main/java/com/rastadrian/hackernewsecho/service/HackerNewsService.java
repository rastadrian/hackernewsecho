package com.rastadrian.hackernewsecho.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rastadrian.hackernewsecho.model.HackerNew;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 9/28/15.
 * @author Adrian Pena
 */
@Component
public class HackerNewsService {
    private static final String URL_STORY_INDEX = "https://hacker-news.firebaseio.com/v0/topstories.json";
    private static final String URL_STORY = "https://hacker-news.firebaseio.com/v0/item/%s.json";
    private static final int STORY_INDEX = 50;

    public List<HackerNew> getTopStories(Integer[] storyIndex) throws IOException {
        List<HackerNew> newsList = new ArrayList<>();

        if(storyIndex != null && storyIndex.length > 0)
        for(Integer integer : storyIndex) {
            HackerNew hackerNew = getStory(integer);
            if(hackerNew != null) {
                newsList.add(hackerNew);
            }
        }
        return newsList;
    }

    public Integer[] getStoryIndex() throws IOException {
        String response = httpGet(URL_STORY_INDEX);
        Integer[] index = new ObjectMapper().readValue(response, Integer[].class);
        Integer[] limitedIndex = new Integer[STORY_INDEX];
        System.arraycopy(index, 0, limitedIndex, 0, STORY_INDEX);
        return limitedIndex;
    }

    private HackerNew getStory(int index) throws IOException {
        String response = httpGet(String.format(URL_STORY, index));
        HackerNew item = new ObjectMapper().readValue(response, HackerNew.class);
        if(item != null) {
            return item;
        }
        return null;
    }

    private String httpGet(String url) {
        HttpHeaders headers = new HttpHeaders();
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> response = template.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
        return response.getBody();
    }
}
