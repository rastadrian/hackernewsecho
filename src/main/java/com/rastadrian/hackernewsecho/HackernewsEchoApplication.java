package com.rastadrian.hackernewsecho;

import com.amazon.speech.speechlet.Speechlet;
import com.amazon.speech.speechlet.servlet.SpeechletServlet;
import com.rastadrian.hackernewsecho.controller.HackerNewsSpeechlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class HackernewsEchoApplication {
    private static final String URL_TEST_SERVLET = "/hn";

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        return new ServletRegistrationBean(createServlet(new HackerNewsSpeechlet()), URL_TEST_SERVLET);
    }

    public static void main(String[] args) {
        SpringApplication.run(HackernewsEchoApplication.class, args);
    }

    private static SpeechletServlet createServlet(final Speechlet speechlet) {
        SpeechletServlet servlet = new SpeechletServlet();
        servlet.setSpeechlet(speechlet);
        return servlet;
    }
}
