package com.rastadrian.hackernewsecho.controller;

import com.rastadrian.hackernewsecho.ApplicationSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on 9/30/15.
 *
 * @author Adrian Pena
 */
@RestController
@RequestMapping("/log")
public class LogController {
    @RequestMapping(
            method = RequestMethod.GET
    )
    public String get() {
        return makeLog();
    }

    private String makeLog() {
        String s = "";
        if(ApplicationSession.log != null) {
            for(String string : ApplicationSession.log) {
                s += "<p>";
                s += string;
                s += "</p>";
            }
        }
        return s;
    }
}
