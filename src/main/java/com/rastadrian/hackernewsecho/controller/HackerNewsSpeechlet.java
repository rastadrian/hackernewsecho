package com.rastadrian.hackernewsecho.controller;

import com.amazon.speech.speechlet.*;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rastadrian.hackernewsecho.ApplicationSession;

/**
 * Created on 9/30/15.
 *
 * @author Adrian Pena
 */
public class HackerNewsSpeechlet implements Speechlet {
    ObjectMapper om = new ObjectMapper();

    @Override
    public void onSessionStarted(SessionStartedRequest sessionStartedRequest, Session session) throws SpeechletException {
        ApplicationSession.addLog("onSessionStarted()");
        try {
            ApplicationSession.addLog(om.writeValueAsString(sessionStartedRequest));
            ApplicationSession.addLog(om.writeValueAsString(session));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public SpeechletResponse onLaunch(LaunchRequest launchRequest, Session session) throws SpeechletException {
        ApplicationSession.addLog("onLaunch()");
        try {
            ApplicationSession.addLog(om.writeValueAsString(launchRequest));
            ApplicationSession.addLog(om.writeValueAsString(session));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return newAskResponse("Hi", "Hi");
    }

    @Override
    public SpeechletResponse onIntent(IntentRequest intentRequest, Session session) throws SpeechletException {
        ApplicationSession.addLog("onIntent()");
        try {
            ApplicationSession.addLog(om.writeValueAsString(intentRequest));
            ApplicationSession.addLog(om.writeValueAsString(session));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return newAskResponse("Hi", "Hi");
    }

    @Override
    public void onSessionEnded(SessionEndedRequest sessionEndedRequest, Session session) throws SpeechletException {
        ApplicationSession.addLog("onSessionEnded()");
        try {
            ApplicationSession.addLog(om.writeValueAsString(sessionEndedRequest));
            ApplicationSession.addLog(om.writeValueAsString(session));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Wrapper for creating the Ask response. The OutputSpeech and {@link Reprompt} objects are
     * created from the input strings.
     *
     * @param stringOutput
     *            the output to be spoken
     * @param repromptText
     *            the reprompt for if the user doesn't reply or is misunderstood.
     * @return SpeechletResponse the speechlet response
     */
    private SpeechletResponse newAskResponse(String stringOutput, String repromptText) {
        PlainTextOutputSpeech outputSpeech = new PlainTextOutputSpeech();
        outputSpeech.setText(stringOutput);

        PlainTextOutputSpeech repromptOutputSpeech = new PlainTextOutputSpeech();
        repromptOutputSpeech.setText(repromptText);
        Reprompt reprompt = new Reprompt();
        reprompt.setOutputSpeech(repromptOutputSpeech);

        return SpeechletResponse.newAskResponse(outputSpeech, reprompt);
    }
}
